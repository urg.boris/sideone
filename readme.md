Simple Rest Api
=================

Instalation
------------

composer install

Create database + import to db migration/structure/sideone.sql 

./config/example.local.neon -> ./config/local.neon + edit db connection

Create new
------------

curl --location 'http://sideone.test/api/person' \
--header 'Content-Type: application/json' \
--header 'Cookie: _nss=1; tracy-session=b6610a993d' \
--data '{
    "name":"Bohumil Harmonicky",
    "phoneNumber":"+6666666612"
}'


Get all or one
------------


curl --location 'http://sideone.test/api/person' \
--header 'Cookie: _nss=1; tracy-session=b6610a993d'


curl --location 'http://sideone.test/api/person/{id}' \
--header 'Cookie: _nss=1; tracy-session=b6610a993d'

Update
------------


curl --location --request PUT 'http://sideone.test/api/person/{id}' \
--header 'Content-Type: application/json' \
--header 'Cookie: _nss=1; tracy-session=b6610a993d' \
--data '{
"name":"Bohumil Disharmonicky",
"phoneNumber":"+555 56565"
}'

Delete
------------

curl --location --request DELETE 'http://sideone.test/api/person/{id}' \
--header 'Cookie: _nss=1; tracy-session=b6610a993d' \
--data ''