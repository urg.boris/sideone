stan:
	php vendor/bin/phpstan analyse -l 9 app --ansi

csf:
	php vendor/bin/phpcbf --standard=ruleset.xml --extensions=php -sp app

cs:
	php vendor/bin/phpcs --standard=ruleset.xml --extensions=php -sp app

rmc:
	php rm -rf ./temp/cache