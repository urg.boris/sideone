<?php declare(strict_types = 1);

namespace App\Model\ORM\Person;

use App\Model\ORM\AbstractMapper;

class PersonMapper extends AbstractMapper
{

	public function getTableName(): string
	{
		return 'person';
	}

}
