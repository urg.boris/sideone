<?php declare(strict_types = 1);

namespace App\Model\ORM\Person;

use App\Model\ORM\AbstractEntity;
use Nextras\Dbal\Utils\DateTimeImmutable;

/**
 * @property int $id {primary}
 * @property string $name
 * @property string $phoneNumber
 * @property DateTimeImmutable $created {default now}
 * @property DateTimeImmutable|NULL $updated
 */
class Person extends AbstractEntity
{

}
