<?php declare(strict_types = 1);

namespace App\Model\ORM\Person;

use App\Model\ORM\AbstractRepository;

class PersonRepository extends AbstractRepository
{

	public static function getEntityClassNames(): array
	{
		return [Person::class];
	}

}
