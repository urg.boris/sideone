<?php declare(strict_types = 1);

namespace App\Model\ORM;

use App\Model\ORM\Person\PersonRepository;
use Nextras\Orm\Model\Model;

/**
 * @property-read PersonRepository $person
 */
class Orm extends Model
{

}
