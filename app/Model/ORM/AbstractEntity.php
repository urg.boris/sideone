<?php declare(strict_types = 1);

namespace App\Model\ORM;

use Nextras\Orm\Entity\Entity;

abstract class AbstractEntity extends Entity
{

}
