<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Model\ORM\Person\Person;
use App\Model\ORM\Person\PersonRepository;
use Nette;
use Nextras\Orm\Exception\NoResultException;

final class ApiPresenter extends Nette\Application\UI\Presenter
{

    public function __construct(
        private PersonRepository $repository,
    )
    {
    }


    /**
     * @throws Nette\Application\AbortException
     */
    private function sendErrorResponse(string $msg): void
    {
        $this->sendJson([
            'error' => $msg,
        ]);
    }

    /**
     * https://www.techiesdiary.com/how-to/how-to-validate-phone-number-using-php
     */


    /*
    private function checkNumberValidity(string $phoneNumber): void
    {
        // Regular expression to match most phone number formats
        $regex = '/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/';
        if (!preg_match($regex, $phoneNumber)) {
            $this->sendErrorResponse(sprintf('%s -invalid phone number', $phoneNumber));
        }
    }
    */

    public function actionPerson(?int $id = null): void
    {
        $httpRequestMethod = $this->getHttpRequest()->getMethod();

        switch ($httpRequestMethod) {
            case 'GET':
                $this->doGetRequest($id);

                break;

            case 'POST':
                $this->doPostRequest();
                break;

            case 'PUT':
                $this->doPutRequest($id);

                break;

            case 'DELETE':
                $this->doDeleteRequest($id);

                break;

            default:
                $this->sendErrorResponse('Method not allowed');
        }
    }

    public function actionDefault(): void
    {
        $this->sendJson([]);
    }

    public function doGetRequest(?int $id): void
    {
        if ($id) {
            try {
                /**
                 * @var Person $person
                 */
                $person = $this->repository->getByIdChecked($id);
                $this->sendJson($person->toArray());
            } catch (NoResultException $e) {
                $this->sendErrorResponse($e->getMessage());
            }
        } else {

            /**
             * @var Person[] $persons
             */
            $persons = $this->repository->findAll()->fetchAll();

            $personsAsArray = array_map(fn(Person $value): array => $value->toArray(), $persons);

            $this->sendJson([
                'persons' => $personsAsArray,
            ]);
        }
    }

    public function doPostRequest(): void
    {
        $data = $this->getHttpRequest()->getRawBody();
        if ($data === null) {
            $this->sendErrorResponse('Missing data');
            return;
        }

        /**
         * @var array<string, string> $personData
         */
        $personData = Nette\Utils\Json::decode($data, true);

        //$this->checkNumberValidity($personData['phoneNumber']);

        $person = new Person();
        $person->name = $personData['name'];
        $person->phoneNumber = $personData['phoneNumber'];

        $this->repository->persistAndFlush($person);

        $this->sendJson($person->toArray());
    }

    public function doPutRequest(?int $id): void
    {
        if (!$id) {
            $this->sendErrorResponse('Missing id');
        } else {
            $data = $this->getHttpRequest()->getRawBody();
            if ($data === null) {
                $this->sendErrorResponse('Missing data');
                return;
            }

            /**
             * @var array<string, string> $personData
             */
            $personData = Nette\Utils\Json::decode($data, true);

            try {

                //$this->checkNumberValidity($personData['phoneNumber']);

                /**
                 * @var Person $person
                 */
                $person = $this->repository->getByIdChecked($id);
                $person->name = $personData['name'];
                $person->phoneNumber = $personData['phoneNumber'];

                $this->repository->persistAndFlush($person);
                $this->sendJson($person->toArray());

            } catch (NoResultException $e) {
                $this->sendErrorResponse($e->getMessage());
            }
        }
    }


    public function doDeleteRequest(?int $id): void
    {
        if (!$id) {
            $this->sendErrorResponse('Missing id');
        } else {
            try {
                /**
                 * @var Person $person
                 */
                $person = $this->repository->getByIdChecked($id);
                $this->repository->removeAndFlush($person);
                $this->sendJson($person->toArray());

            } catch (NoResultException $e) {
                $this->sendErrorResponse($e->getMessage());
            }
        }
    }

}
